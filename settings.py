# Хранение настроек игры
import pygame as pg


class Settings:
    """Класс для хранения всех настроек игры Alien Invasion"""
    def __init__(self):
        self.__screen_width = 1200  # ширина окна
        self.__screen_height = 700  # высота окна
        self.__pic_of_background = "image\\space.jpg"  # фоновая картинка
        self.__background = pg.transform.scale(
            pg.image.load(self.__pic_of_background), (self.__screen_width, self.__screen_height))
        self.__speed_of_ship = 6.5
        self.__bullet_width = 3
        self.__bullet_height = 10
        self.__bullet_color = (0, 255, 0)
        self.__bullet_speed = 7.5
        self.__bullets_allowed = 3
        self.__alien_speed = 4
        self.__alien_down_speed = 5
        self.__alien_direction = 1  # 1 - направление вправо, -1 - влево
        self.__hp = 3
        self.__active_game = True

    def get_screen_width(self):
        return self.__screen_width

    def get_screen_height(self):
        return self.__screen_height
    
    def get_pic_of_background(self):
        return self.__pic_of_background

    def get_background(self):
        return self.__background
    
    def get_speed_of_ship(self):
        return self.__speed_of_ship
    
    def get_bullet_width(self):
        return self.__bullet_width
    
    def get_bullet_height(self):
        return self.__bullet_height

    def get_bullet_color(self):
        return self.__bullet_color
    
    def get_bullet_speed(self):
        return self.__bullet_speed

    def get_bullets_allowed(self):
        return self.__bullets_allowed
    
    def get_alien_speed(self):
        return self.__alien_speed

    def get_alien_down_speed(self):
        return self.__alien_down_speed
    
    def get_alien_direction(self):
        return self.__alien_direction

    def get_hp(self):
        return self.__hp

    def get_active_game(self):
        return self.__active_game

    def set_alien_direction(self, flag):
        self.__alien_direction *= flag
    
    def set_hp(self, hp):
        self.__hp = hp

    def set_active_game(self, flag):
        self.__active_game = flag
