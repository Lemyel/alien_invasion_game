import sys  # завершает игру по команде пользователя
import pygame as pg
import bullet as bull
import alien as al
import ship as sh


def check_events(game_st, screen, ship, bullets):
    """Обработка нажатия клавиш и события мыши"""
    # pg.event.get() получает доступ к событиям
    # далее в блоках обрабатываем событие
    for event in pg.event.get():
        # описание первого события - если пользователь нажимает на закрытие окна
        # то вызывается метод sys.exit() для выхода из игры
        if event.type == pg.QUIT:
            sys.exit()
        # отслеживаем нажатие клавиатуры
        elif event.type == pg.KEYDOWN:
            chek_keydown_events(event, game_st, screen, ship, bullets)
        # отслеживаем отпускание клавиатуры
        elif event.type == pg.KEYUP:
            check_keyup_events(event, ship)


def update_screen(game_st, screen, ship, bullets, aliens, hp):
    """Обновление экрана"""
    # при каждом проходе цикла перерисовываем экран
    # screen.fill(bg_color) # установка цвета экрана
    screen.blit(game_st.get_background(), (0, 0))  # установка фоновой картинки
    draw_bullets(bullets)
    ship.my_blit()
    draw_aliens(aliens)
    draw_hp(hp)
    # мы получили и обработали событие, теперь отобржаем последний отрисованный экран
    # предыдущий экран стирается    
    pg.display.update()


def chek_keydown_events(event, game_st, screen, ship, bullets):
    """Реагирование на нажатие клавиш"""
    if event.key == pg.K_RIGHT:
        ship.set_moving_right(True)
    if event.key == pg.K_LEFT:
        ship.set_moving_left(True)
    if event.key == pg.K_UP:
        ship.set_moving_up(True)
    if event.key == pg.K_DOWN:
        ship.set_moving_down(True)   
    if event.key == pg.K_SPACE:
        fire_bullets(bullets, game_st, screen, ship)
    if event.key == pg.K_q:
        sys.exit()    


def check_keyup_events(event, ship):
    """Реагирование на отпускание клавиш"""
    if event.key == pg.K_RIGHT:
        ship.set_moving_right(False)
    if event.key == pg.K_LEFT:
        ship.set_moving_left(False)
    if event.key == pg.K_UP:
        ship.set_moving_up(False)
    if event.key == pg.K_DOWN:
        ship.set_moving_down(False)


def fire_bullets(bullets, game_st, screen, ship):
    if len(bullets) + 1 <= game_st.get_bullets_allowed():
        new_bullet = bull.Bullet(game_st, screen, ship)
        bullets.add(new_bullet)    


def draw_bullets(bullets):
    for b in bullets:
        b.draw_bullet()


def draw_aliens(aliens):
    for a in aliens:
        a.my_blit()


def draw_hp(hp):
    for h in hp:
        h.my_blit()


def update_aliens(game_st, screen, ship, aliens, bullets):
    """Проверяет достиг ли флот края окна и обновляет позиции пришельцев"""
    check_aliens_die(game_st, screen, ship, aliens, bullets)
    check_fleet_edges(game_st, aliens)
    aliens.update()


def check_fleet_edges(game_st, aliens):
    """Реагируем на достижение пришельцем края экрана"""
    for alien in aliens:
        if alien.check_edges():
            change_fleet_direction(game_st, aliens)
            break


def change_fleet_direction(game_st, aliens):
    """Опускает весь флот и меняет направление"""
    for alien in aliens:
        alien.get_rect().y += game_st.get_alien_down_speed()
    game_st.set_alien_direction(-1)


def update_hp(game_st, screen, ship, aliens, hp):
    check_ship_die(game_st, screen, ship, aliens, hp)
    check_hp(game_st, screen)
    if len(hp) == 0:
        for i in range(0, game_st.get_hp()):
            create_hp(game_st, screen, hp, i)
    hp.update()


def check_hp(game_st, screen):
    if game_st.get_hp() <= 0:
        game_st.set_active_game(False)
        game_stop(game_st, screen)
        pg.display.update()


def create_hp(game_st, screen, hp, i):
    health = sh.Hp(game_st, screen)
    hp_width = health.get_rect().width
    hp_height = health.get_rect().height

    health.set_rect_x(hp_width * 0.25 + 1.25 * hp_width * i)
    health.set_rect_y(screen.get_rect().height - hp_height * 1.5)
    hp.add(health)


def check_aliens_die(game_st, screen, ship, aliens, bullets):
    """Проверка на столкновнение пули с пришельцем"""
    for alien in aliens:
        for bullet in bullets:
            if alien.get_rect().colliderect(bullet.get_rect()):
                aliens.remove(alien)
                bullets.remove(bullet)
    if len(aliens) == 0:
        create_fleet(game_st, screen, ship, aliens)


def create_fleet(game_st, screen, ship, aliens):
    """Создание флота пришельцев"""
    alien = al.Alien(game_st, screen)
    number_aliens_x = get_number_cols(game_st, alien.get_rect().width)
    number_aliens_y = get_number_rows(game_st, ship.get_rect().height, alien.get_rect().height)
    # создание флота
    for row in range(number_aliens_y):
        for col in range(number_aliens_x):
            create_alien(game_st, screen, aliens, col, row)


def create_alien(game_st, screen, aliens, col, row):
    alien = al.Alien(game_st, screen)
    alien_width = alien.get_rect().width
    alien_height = alien.get_rect().height
    # позиционирование пришельца
    # alien_width * 0.25 и alien_height * 0.25 обозначают отступы слева и сверху
    # 1.5 * alien_width * col отступ от следующего приешельца от предыдущего
    alien.set_rect_x(alien_width * 0.25 + 1.5 * alien_width * col)
    alien.set_rect_y(alien_height * 0.25 + 1.5 * alien_height * row)
    aliens.add(alien)


def check_ship_die(game_st, screen, ship, aliens, hp):
    """Проверка на столкновение корабля с пришельцем и
        не дошел ли пришелец до самого низа"""
    for alien in aliens:
        if alien.get_rect().colliderect(ship.get_rect()):
            game_st.set_hp(game_st.get_hp() - 1)
            ship_start_position(ship, screen)
            aliens.remove(alien)
            hp.remove(hp.sprites()[-1])
            break
        if alien.get_rect().bottom >= screen.get_rect().bottom:
            game_st.set_hp(-1)
            break


def ship_start_position(ship, screen):
    """При столкновении пришельца с кораблем, возращаем корабль
        в начальную позицию"""
    ship.get_rect().centerx = screen.get_rect().centerx 
    ship.get_rect().bottom = screen.get_rect().bottom


def get_number_cols(game_st, alien_width):
    """Получения количества пришельцев в ряду"""
    # измеряем свободное пространство(отступы от краев делаются при их позиционировании)
    available_space_x = game_st.get_screen_width() - alien_width * 0.5
    # 1.5 * alien_width = ширина для 1 пришельца: сам пришелец и половина от его ширины 
    number_cols = int(available_space_x / (1.5 * alien_width))
    return number_cols


def get_number_rows(game_st, ship_height, alien_height):
    """Получение количества рядов"""
    n = 4  # количество пустых строк перед кораблем
    available_space_y = game_st.get_screen_height() - n * alien_height - ship_height
    number_rows = int(available_space_y / (1.5 * alien_height))
    return number_rows


def game_stop(game_st, screen):
    """Отображение конечных результатов игры"""
    if not game_st.get_active_game():
        font = pg.font.Font(pg.font.get_default_font(), 35)
        text = font.render("Игра закончена", True, (0, 0, 0))
        screen.blit(text, [game_st.get_screen_width()/2 - text.get_rect().width / 2,
                           game_st.get_screen_height()/2 - text.get_rect().height / 2])
