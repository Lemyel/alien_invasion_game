# класс пришельцев

import pygame as pg
from pygame.sprite import Sprite


class Alien(Sprite):
    def __init__(self, game_st, screen):
        """Инициализация пришельца"""
        super().__init__()
        self.__screen = screen
        self.__game_st = game_st

        self.__image = pg.transform.scale(pg.image.load("image\\ufo.png"),
                                          (int(screen.get_rect()[2]*0.07), int(screen.get_rect()[3]*0.06)))
        self.__rect = self.__image.get_rect()
        self.__rect.y = 0
        self.__rect.x = 0
    
    def my_blit(self):
        self.__screen.blit(self.__image, self.__rect)
    
    def update(self):
        """Перемещает пришельца вправо"""        
        self.__rect.x += (self.__game_st.get_alien_speed() *
                          self.__game_st.get_alien_direction())
        
    def check_edges(self):
        """Возращает True, если пришелец достиг края"""
        screen_rect = self.__screen.get_rect()
        if self.__rect.right >= (screen_rect.right - self.__game_st.get_alien_speed()):
            return True
        elif self.__rect.left <= self.__game_st.get_alien_speed():
            return True

    def get_rect(self):
        return self.__rect

    def set_rect_x(self, x):
        self.__rect.x = x

    def set_rect_y(self, y):
        self.__rect.y = y
