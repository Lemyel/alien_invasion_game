# класс реализующий стрельбу
import pygame as pg
from pygame.sprite import Sprite


class Bullet(Sprite):
    """Класс для управления пулями, выпущенными кораблем"""

    def __init__(self, game_st, screen, ship):
        """Создание обьекта пули в текущей позиции корабля"""
        super().__init__()
        self.__screen = screen

        #  создание пули в определенной позиции
        self.__rect = pg.Rect(ship.get_rect().centerx, ship.get_rect().top,
                              game_st.get_bullet_width(), game_st.get_bullet_height())
        
        self.__color = game_st.get_bullet_color()
        self.__bullet_speed = int(game_st.get_bullet_speed())
        # self.__moving_space = False

    # для группы переопределяем функцию update, потому что у нас свой смысл в этой функции
    def update(self, bullets):
        """перемещает пулю по экрану"""
        self.__rect.y -= self.__bullet_speed
        # пули за границей экрана удаляем из группы bullets
        for b in bullets:
            if b.get_rect().bottom <= self.__screen.get_rect().top:
                bullets.remove(b)    

    def draw_bullet(self):
        """Вывод пули на экран"""
        pg.draw.rect(self.__screen, self.__color, self.__rect)
    
    def get_rect(self):
        return self.__rect
