import pygame as pg
from pygame.sprite import Group
import settings as st
import ship as sh 
import game_functions as gf


def run_game():
    """Инициализация  игры"""
    pg.init()  # базовый метод, который инициализирует настройки Pygame(инициализируем движок)
    game_st = st.Settings()
    size_of_screen = (game_st.get_screen_width(), game_st.get_screen_height())
    # создаем игровое окно - поверхность
    screen = pg.display.set_mode(size_of_screen) 
    # название окна
    pg.display.set_caption("Alien Invision") 
    # Цвет, которым будет заливаться экран
    # bg_color = (255, 255, 0)
    ship = sh.Ship(screen)
    # нужна группа потому что предыдущие выстрелы тоже необходимо сохранять
    # иначе бы при обновлении экрана они удалялись
    bullets = Group()
    aliens = Group()
    hp = Group()

    clock = pg.time.Clock()
    fps = 75

    while True:  # постоянно обновляем окно, пока пользователь сам не завершит
        """Отслеживание событий клавиатуры и мыши"""
        # ремарка: когда мы бередаем bullets, при нажатии пробел он запонлняется
        # экземплярами из класса Bullet
        gf.check_events(game_st, screen, ship, bullets)
        ship.update(game_st)
        bullets.update(bullets)
        gf.update_aliens(game_st, screen, ship, aliens, bullets)
        gf.update_hp(game_st, screen, ship, aliens, hp)
        if game_st.get_active_game():
            gf.update_screen(game_st, screen, ship, bullets, aliens, hp)

        clock.tick(fps)


run_game()
