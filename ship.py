# реализация поведения корабля
import pygame as pg
from pygame.sprite import Sprite


class Ship:
    """Инициализация корабля"""
    def __init__(self, screen):
        self.__screen = screen

        # Загрущка изображения корабля и получение прямоугольника
        # размер корабля зависит от размеров окна 5 и 15 % в ширину и высоту соответственно
        self.__image = pg.transform.scale(pg.image.load("image\\ship.png"),
                                          (int(screen.get_rect()[2]*0.06), int(screen.get_rect()[3]*0.11)))
        # прямоугольник соответствующий кораблю
        self.__rect = self.__image.get_rect()
        # прямоугольник соответствующий окну
        self.__screen_rect = screen.get_rect()
        # Каждый новый корабль появляется по центру нижнего края экрана
        # центр прямоугольника корабля по Х будет по центру по Х окна
        self.__rect.centerx = self.__screen_rect.centerx 
        # нижний край будет край корабля у нижнего края окна
        self.__rect.bottom = self.__screen_rect.bottom
        # Флаг непрерыного перемещения
        self.__moving_right = False
        self.__moving_left = False
        self.__moving_up = False
        self.__moving_down = False

    def update(self, game_st):
        """Перемещение корабля"""
        speed_of_ship = int(game_st.get_speed_of_ship())
        if (self.__moving_right and
                (self.__rect.right + speed_of_ship <= self.__screen_rect.right)):
            self.__rect.centerx += speed_of_ship

        if (self.__moving_left and
                (self.__rect.left - speed_of_ship >= self.__screen_rect.left)):
            self.__rect.centerx -= speed_of_ship

        if (self.__moving_up and
                (self.__rect.top - speed_of_ship >= self.__screen_rect.top)):
            self.__rect.centery -= speed_of_ship

        if (self.__moving_down and
                (self.__rect.bottom + speed_of_ship <= self.__screen_rect.bottom)):
            self.__rect.centery += speed_of_ship

    def my_blit(self):
        """рисует корабль в текущей позиции"""
        self.__screen.blit(self.__image, self.__rect)

    def set_moving_right(self, flag):
        self.__moving_right = flag

    def set_moving_left(self, flag):
        self.__moving_left = flag

    def set_moving_up(self, flag):
        self.__moving_up = flag

    def set_moving_down(self, flag):
        self.__moving_down = flag
    
    def get_rect(self):
        return self.__rect


class Hp(Sprite):
    """Инициализация здровоья"""
    def __init__(self, game_st, screen):
        super().__init__()
        self.__screen = screen
        self.__game_st = game_st
        # сердечки(hp) корабля
        self.__image_hp = pg.transform.scale(pg.image.load("image\\hp.png"),
                                             (int(screen.get_rect()[2] * 0.012), int(screen.get_rect()[3] * 0.018)))
        self.__rect_hp = self.__image_hp.get_rect()

    def get_rect(self):
        return self.__rect_hp

    def my_blit(self):
        """рисует сердечки"""
        self.__screen.blit(self.__image_hp, self.__rect_hp)

    def set_rect_x(self, x):
        self.__rect_hp.x = x

    def set_rect_y(self, y):
        self.__rect_hp.y = y
